// Sidebar Toggle
$(document).ready(function () {

  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
      $('.burger').toggleClass.toggle('open');
  });

});

$('.burger').click(function () {
  $('.burger').toggleClass('open');
});

// Music Player
var currentTrack = 0;


var playlist = [
  "./audio/broken_mirrors.mp3",
  "./audio/improvised_fight.mp3",
  "./audio/lonley_ropes.mp3",
  "./audio/miss_direction.mp3",
  "./audio/no_diggity.mp3",
  "./audio/sun_up_sun_down.mp3",
];


var audioPlayer = document.querySelector('#player');

// Track Controls
$('#playlist p').click(function(e){
  e.preventDefault();

  currentTrack = Number( $(this).attr('data-track') ) + 1;
  playNext();
});


$('#nextBtn').click(function(){

  $('#playlist p').removeClass('playing');

  // console.log(currentTrack, playlist.length  - 1);
  if(currentTrack < playlist.length - 1) {
    currentTrack = currentTrack + 1;
    $('#player').attr('src', playlist[currentTrack]);
    audioPlayer.play();
  } else {
    currentTrack = 0;
    $('#player').attr('src', playlist[currentTrack]);
    audioPlayer.play();
  }

  $( "#playlist p[data-track="+currentTrack+"]" ).addClass('playing');

});

$('#prevBtn').click(function(){
  // console.log(currentTrack, playlist.length  - 1);
  $('#playlist p').removeClass('playing');
  playNext();
  $( "#playlist p[data-track="+currentTrack+"]" ).addClass('playing');
});


function playNext() {
  if(currentTrack > 0) {
    currentTrack = currentTrack - 1;
    $('#player').attr('src', playlist[currentTrack]);
    audioPlayer.play();
    $('#playBtn').hide();
    $('#pauseBtn').show();
  } else {
    currentTrack = playlist.length  - 1;
    $('#player').attr('src', playlist[currentTrack]);
    audioPlayer.play();
    $('#playBtn').hide();
    $('#pauseBtn').show();
  }
}


$('#pauseBtn').click(function() {
  audioPlayer.pause();
  $(this).hide();
  $('#playBtn').show();
  $("#status").text("Status: Paused");
});

$('#playBtn').click(function() {
  // audioPlayer.currentTime = 300;
  audioPlayer.play($('#pauseBtn').show());
  $(this).hide();
  // $('#pauseBtn').show();
  $("#status").text("Status: Playing");
});


// Volume
audioPlayer.volume = 0.2;

function setVolume() {
  audioPlayer.volume = volume.value / 100;
}


// Progress Bar + Time Update
audioPlayer.addEventListener("timeupdate", function() {
    
    var curmins = Math.floor(audioPlayer.currentTime / 60);
    var cursecs = Math.floor(audioPlayer.currentTime - curmins * 60);

    var durmins = Math.floor(audioPlayer.duration / 60);
    var dursecs = Math.floor(audioPlayer.duration - durmins * 60);
       if(cursecs < 10){ cursecs = "0"+cursecs; }
       if(dursecs < 10){ dursecs = "0"+dursecs; }
      
        if (isNaN(durmins)) {
          $('#duration').text("0"+":"+"00");
          $('#current-time').text("0"+":"+"00");
        } else {
          $('#duration').text(durmins+":"+dursecs);
          $('#current-time').text(curmins+":"+cursecs);
        }

    var progress = ((audioPlayer.currentTime / audioPlayer.duration) * 100).toFixed(1);
    // console.log(progress);0

    if (progress >= 100) {
      playNext();
      $('.bar').css('width', '0%');
    } else {
      $('.bar').css('width', progress+'%');
    }

});


// Currently Playing
$(".song").click(function() {
  var selected = $(this).hasClass("playing");
    $(".song").removeClass("playing");
      if(!selected)
        $(this).addClass("playing");
});









